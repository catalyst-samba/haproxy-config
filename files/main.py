#!/usr/bin/env python
"""
HAProxy Auto Config Tool

This script will find all LDAP servers in current site, and render haproxy.cfg.
"""
from __future__ import unicode_literals
import io
import sys
import logging
import argparse
import subprocess
from os.path import abspath, dirname, join, exists

LOG_FORMAT = '%(levelname)s %(asctime)s #%(lineno)d: %(message)s'

LDAPS_PORT = 636

HERE = dirname(abspath(__file__))
CLDAP_SCRIPT_PATH = join(HERE, 'cldap.pl')
ROOT_CA_FILE_DEFAULT = '/etc/ssl/haproxy/rootCA.pem'
CERT_AND_KEY_FILE_DEFAULT = '/etc/ssl/haproxy/cert-and-key.pem'

HAPROXY_CFG_TEMPLATE = """
global
    log /dev/log    local0
    log /dev/log    local1 notice
    chroot /var/empty/haproxy
    stats socket /var/run/haproxy.sock mode 660 level admin
    pidfile /var/run/haproxy.pid
    stats timeout 30s
    user haproxy
    group haproxy
    daemon

    # Default ciphers to use on SSL-enabled listening sockets.
    # For more information, see ciphers(1SSL). This list is from:
    #  https://hynek.me/articles/hardening-your-web-servers-ssl-ciphers/
    ssl-default-bind-ciphers ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:ECDH+3DES:DH+3DES:RSA+AESGCM:RSA+AES:RSA+3DES:!aNULL:!MD5:!DSS
    ssl-default-bind-options no-sslv3

    ssl-default-server-ciphers ECDH+AESGCM:ECDH+CHACHA20:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:RSA+AESGCM:RSA+AES:!aNULL:!MD5:!DSS
    ssl-default-server-options no-sslv3

    tune.ssl.default-dh-param 2048

defaults
    log       global
    mode      tcp
    option    tcplog
    option    dontlognull
    timeout connect 5000
    timeout client  50000
    timeout server  50000

backend ldaps_backend
    balance roundrobin
    {server_config_lines}

frontend ldaps_frontend
    {bind_config_line}
    default_backend ldaps_backend
"""


class HaproxyConfig:

    def __init__(
            self,
            realm='',
            root_ca_file='',
            cert_and_key_file='',
            nameserver='',
            verbose=False,
            quiet=False,
            cfg_path='/etc/haproxy/haproxy.cfg'):

        self.realm = realm
        self.root_ca_file = root_ca_file
        self.cert_and_key_file = cert_and_key_file

        self.nameserver = ''
        if nameserver and nameserver.lower() != 'default':
            self.nameserver = nameserver

        self.cfg_path = cfg_path
        self.tmp_cfg_path = '/tmp/haproxy.cfg'
        self.logger = self.get_logger(verbose=verbose, quiet=quiet)

    def get_logger(self, verbose=False, quiet=False):
        logger = logging.getLogger(__name__)

        level = ((verbose and logging.DEBUG) or
                 (quiet and logging.WARN) or logging.INFO)

        logger.setLevel(level)

        formatter = logging.Formatter(fmt=LOG_FORMAT)

        handler = logging.StreamHandler()
        handler.setFormatter(formatter)
        logger.addHandler(handler)
        return logger

    def run_cmd(self, cmd, title='run cmd'):

        self.logger.debug('%s:\n\n%s\n', title, cmd)

        if not isinstance(cmd, list):
            cmd = cmd.split()

        try:
            bytes_output = subprocess.check_output(cmd)
        except subprocess.CalledProcessError as err:
            msg = err.output.decode('utf8')
            self.logger.error(msg)
            sys.exit(err.returncode)

        output = bytes_output.decode('utf8').strip()
        self.logger.debug('cmd output:\n\n%s\n\n', output)
        return output

    def run_cmd_host(self, domain_name,
                     query_type='SRV', title='run host cmd'):
        cmd = 'host -t {} {} {}'.format(
            query_type, domain_name, self.nameserver)
        # nameserver can be empty, rstrip it.
        return self.run_cmd(cmd.rstrip(), title=title)

    def get_pdc_server(self):
        """
        Get Primary Domain Controller hostname from DNS.
        """
        self.logger.info('current realm: %s', self.realm)
        domain_name = '_ldap._tcp.pdc._msdcs.{}'.format(self.realm)
        output = self.run_cmd_host(
            domain_name, title='query DNS for PDC server')
        self.pdc_server = output.strip().split()[-1].strip('.').lower()
        self.logger.info('find PDC server via realm: %s', self.pdc_server)

    def get_site_name(self):
        """
        Get current site name with cldap.pl script.

        Talk with PDC server, find `Client Site Name` in response.
        """
        cmd = [CLDAP_SCRIPT_PATH, '--server', self.pdc_server]
        output = self.run_cmd(cmd, title='run cldap.pl to get site name')
        for line in output.splitlines():
            line = line.strip()
            if line and line.startswith('Client Site Name'):
                # somehow there is a \t and dozens of \x00 before site name
                self.site_name = line.split(':')[-1].strip().strip('\x00')
                self.logger.info('find current site name: %s', self.site_name)
                return

    def get_ldap_servers_in_site(self):
        """
        Get all ldap servers in current site.
        """
        domain_name = '_ldap._tcp.{}._sites.{}'.format(
            self.site_name, self.realm)
        output = self.run_cmd_host(
            domain_name, title='query DNS for ldap servers')
        self.ldap_servers = []
        for line in output.strip().splitlines():
            if line.startswith(domain_name):
                server = line.strip().split()[-1].strip('.').lower()
                if server:
                    self.logger.info('find ldap server: %s', server)
                    self.ldap_servers.append(server)

    def _format_server_config_line(
            self, name, address, port=LDAPS_PORT, settings=''):
        return 'server {} {}:{} check-ssl ssl ca-file {} {}'.format(
            name, address, port, self.root_ca_file, settings).strip()

    def _get_server_config_lines(self):
        lines = []
        for index, server in enumerate(self.ldap_servers):
            line = self._format_server_config_line(
                'svr{}'.format(index), server)
            lines.append(line)
        # add pdc as backup
        if self.pdc_server not in self.ldap_servers:
            pdc_line = self._format_server_config_line(
                'pdc', self.pdc_server, settings='backup')
            lines.append(pdc_line)
        return '\n    '.join(lines)

    def _get_bind_config_line(self):
        return 'bind :{} ssl crt {}'.format(
            LDAPS_PORT, self.cert_and_key_file)


    def render_config(self):
        context = {
            'server_config_lines': self._get_server_config_lines(),
            'bind_config_line': self._get_bind_config_line(),
        }
        self.tmp_cfg_text = HAPROXY_CFG_TEMPLATE.format(**context).strip()
        self.logger.debug("render config:\n\n%s\n\n", self.tmp_cfg_text)
        with io.open(self.tmp_cfg_path, mode='w', encoding='utf8') as cfg_file:
            cfg_file.write(self.tmp_cfg_text)

    def is_config_same(self):
        try:
            with io.open(self.cfg_path, mode='r', encoding='utf8') as cfg_file:
                cfg_text = cfg_file.read()
                return cfg_text == self.tmp_cfg_text
        except IOError:
            return False

    def write_config(self):
        with io.open(self.cfg_path, mode='w', encoding='utf8') as cfg_file:
            cfg_file.write(self.tmp_cfg_text)

    def validate_config(self):
        cmd = 'haproxy -c -V -f {}'.format(self.tmp_cfg_path)
        return self.run_cmd(cmd)

    def reload_config(self):
        cmd = 'sudo systemctl reload haproxy'
        self.run_cmd(cmd)

    def show_status(self):
        cmd = 'sudo systemctl status --full haproxy'
        self.run_cmd(cmd)

    def run(self):
        """
        Template method to run all
        """
        self.get_pdc_server()
        self.get_site_name()
        self.get_ldap_servers_in_site()
        self.render_config()
        if self.is_config_same():
            self.logger.info('no change in config, skip')
        else:
            self.validate_config()
            self.write_config()
            self.reload_config()
            self.show_status()


def main():

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description='HAProxy Auto Configuration')

    parser.add_argument('realm', metavar='REALM', help='Domain Realm')

    parser.add_argument('--root-ca-file',
                        default=ROOT_CA_FILE_DEFAULT,
                        help='Path to root CA cert file'),
    parser.add_argument('--cert-and-key-file',
                        default=CERT_AND_KEY_FILE_DEFAULT,
                        help=('Path to a PEM file containing both the '
                              'required certificates and any associated '
                              'private keys'))

    parser.add_argument('-n', '--nameserver',
                        default='',
                        help='Optioanl DNS nameserver to query against')

    parser.add_argument('-v', '--verbose',
                        action='store_true',
                        help='Be verbose, print debug messages')

    parser.add_argument('-q', '--quiet',
                        action='store_true',
                        help='Be quiet, only print warnings and errors')

    args = parser.parse_args()

    HaproxyConfig(
        nameserver=args.nameserver,
        realm=args.realm,
        root_ca_file=args.root_ca_file,
        cert_and_key_file=args.cert_and_key_file,
        verbose=args.verbose,
        quiet=args.quiet,
    ).run()


if __name__ == '__main__':
    main()
