# HAProxy Auto Config Tool with Ansible

This is an Ansible project which will:

- setup HAProxy on Linux to proxy LDAP servers over SSL/TLS in your domain site
- deploy a Python script and its friends to server, run as cron job to auto config HAProxy

# Prerequisites

## Create a server for HAProxy in your site

- So far, only Ubuntu 18.04 and CentOS 7 are tested.
- The server should have python available(which should be there by default).
- A user can ssh to server with a private key, and can use `sudo` without password.
- The DNS on server must be able to resolve primary DC and LDAP servers.

## PEM files for SSL/TLS

All traffic are over SSL/TLS.

We assume your organization has a root CA certificate for all server keys.
An example file is at `ssl/rootCA.pem`.

Also, you need to allocate a domain name(e.g.: ldap.samdom.example.com),
and generate a matched cert and key pair for HAProxy server.
Please concat the cert and key into one single file.
An example file is at `ssl/cert-and-key.pem`.
This file is used as `crt` for frontend bind options in `haproxy.cfg`.

Once you get your ca and crt pem files ready, there are 3 ways to use them:

- drop them into the `ssl` dir and overwrite the example ones.
- put them anywhere and adjust `vars` in `main.yml`.
- provide vars to ansible with command line options.

For more details, refer to:
[HAProxy configuration: bind options: crt](https://cbonte.github.io/haproxy-dconv/1.8/configuration.html#5.1-crt)

## Setup local Python environment to run Ansible

Create a virtualenv and install requirements.txt:

    virtualenv ansible
    source ansible/bin/activate
    pip install -U -r requirements.txt

Note: Ansible requires Python 2.6+ or 3.5+.

## Create your inventory file

Create your Ansible inventory file with correct vars to connect HAProxy server.
You can copy our testing `vagrant.ini` and modify:

    copy vagrant/vagrant.ini hosts.ini
    vim hosts.ini  # modify host ip, user name, private key, etc

`hosts.ini` will be default inventory as specified in `ansible.cfg`.
And it's ignored by git in `.gitignore`.

Verify your setup:

    ./main.yml -i hosts.ini --tags ping

For more details about Ansible inventory, refer to:
[Ansible: Working with Inventory](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html)

## Adjust vars in playbook

You can edit `vars` in `main.yml` to match your environment:

    vim main.yml
    # change realm and nameserver, etc

Note: the `nameserver` is only used for `host` command in `files/main.py`,
This tool didn't change `/etc/resolv.conf` on server so far, since this may
cause other problems. Please make sure DNS is working properly on HAProxy
server yourself.

You can also overwrite vars from Ansible command line options with `-e name=value`.
See example bellow.

## Run Ansible playbook

Run the playbook:

    ./main.yml               # all default
    ./main.yml -v            # verbose, try -vv, -vvv, -vvvv to get more
    ./main.yml -i hosts.ini  # specify inventory explicitly
    # overwrite vars via command line options
    ./main.yml -e realm=SAMDOM.EXAMPLE.COM -e nameserver=10.10.10.12 -e ca_file_src=/path/to/ca -e crt_file_src=/path/to/crt ...

## Verify proxy

If your domain name for HAProxy is `ldap.samdom.example.com`:

    LDAPTLS_REQCERT=hard LDAPTLS_CACERT=files/rootCA.pem ldapsearch -H ldaps://ldap.samdom.example.com -s base -x

This command should return you LDAP information with SSL/TLS required.
If you run it multiple times, it should hit different LDAP servers in your site in roundrobin.

Also, a cron job should have been setup on HAProxy server, you can ssh to server and check:

    sudo crontab -l


## How it works

The Ansible playbook just setup the HAProxy server and deploy the `main.py` script,
you should only need to run it once and forget it unless upgrading. But it's ok to run it multiple times.

Main tasks are done in `main.py`, which will do following things in order.

It takes the `realm` and `nameserver` args, then use `host` command to find primary DC in domain:

    host -t SRV _ldap._tcp.pdc._msdcs.SAMDOM.EXAMPLE.COM 10.10.10.12

Note: if DNS in `/etc/resolv.conf` is working fine, the nameserver IP is optional.

We should get the PDC server host name with such response:

    Using domain server:
    Name: 10.10.10.12
    Address: 10.10.10.12#53
    Aliases:

    _ldap._tcp.pdc._msdcs.SAMDOM.EXAMPLE.COM has SRV record 0 100 389 s1.samdom.example.com.

Then it will talk to above PDC server using `cldap.pl` script:

    ./cldap.pl --server s1.samdom.example.com

which should tell us current site name in response like this:

    ...
    Client Site Name:   Default-First-Site-Name
    ...

Now, with site name and realm, we can ask DNS for all LDAP servers in site:

    host -t SRV _ldap._tcp.Default-First-Site-Name._sites.SAMDOM.EXAMPLE.COM 10.10.10.12

The response should be like this:

    Using domain server:
    Name: 10.10.10.12
    Address: 10.10.10.12#53
    Aliases: 

    _ldap._tcp.Default-First-Site-Name._sites.SAMDOM.EXAMPLE.COM has SRV record 0 100 389 s1.samdom.example.com.
    _ldap._tcp.Default-First-Site-Name._sites.SAMDOM.EXAMPLE.COM has SRV record 0 100 389 s2.samdom.example.com.

Then it uses above LDAP server list to render `haproxy.cfg`, and uses PDC server as backup if it's not in the list(which implies it's not in current site).
Note: HAProxy needs to check the availability for each server, so DNS must be able to resolve each DNS host name.

The above tasks will be executed in a cron job(every 5 mins by default).
If nothing changed, the script will do nothing.
Otherwise haproxy.cfg will be overwritten and reloaded.

## Trouble shooting

If you get any trouble, first please check the DNS is working properly on the HAProxy server.
You can ssh to server to try above commands manually, or trigger `main.py` with verbose logs:

    sudo ./main.py -v --nameserver 10.10.10.12 SAMDOM.EXAMPLE.COM [...]

It should print errors if anything goes wrong.

Note: `main.py` has to be executed with sudo, since it needs to write config into `/etc/haproxy/haproxy.cfg` and reload haproxy service.

The other potential issue could be caused by PEM files, you can check them by:

    openssl x509 -in cert-and-key.pem -noout -text
    openssl x509 -in rootCA.pem -noout -text

To collect logs from your localhost:

    ./main.yml -v | tee ansible.log

Or on HAProxy server:

    sudo systemctl status haproxy.service | tee systemctl.log
    sudo ./main.py -v --nameserver 10.10.10.12 SAMDOM.EXAMPLE.COM | tee main.log
